#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/9/1 14:16
# @Author  : Yunnigu
# @File    : ximalaya.py

import time
import os
import base64
import requests
from urllib import parse

session = requests.session()


def get_list(albumId, page_id):
    timestamp = str(int(time.time()*1000))
    url = 'http://mobile.ximalaya.com/mobile-album/album/page/ts-{}?ac=WIFI&albumId={}&device=android&isAsc=true&isQueryInvitationBrand=true&isVideoAsc=true&pageId={}&pageSize=20&pre_page=0&source=3&supportWebp=true'.format(timestamp, albumId, str(page_id))
    headers = {
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-cn',
        'User-Agent': 'ting_6.7.3(ONEPLUS+A5000,Android25)'
    }
    response = session.get(url=url, headers=headers)
    print(response.text)
    return response.json()



def get_list1(albumId, page_id):
    timestamp = str(int(time.time()*1000))
    url = 'http://mobile.ximalaya.com/mobile/v1/album/track/ts-{}?albumId={}&device=android&isAsc=true&isQueryInvitationBrand=true&pageId={}&pageSize=20&pre_page=0'.format(timestamp, albumId, str(page_id))
    headers = {
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-cn',
        'User-Agent': 'ting_6.7.3(ONEPLUS+A5000,Android25)'
    }
    response = session.get(url=url, headers=headers)
    print(response.text)
    return response.json()



def get_downloadurl(track_id):
    timestamp = str(int(time.time()*1000))
    url = 'https://mpay.ximalaya.com/mobile/track/pay/download/{}/ts-{}?device=android&trackQualityLevel=1&track_id={}'.format(track_id, timestamp, track_id)
    headers = {
        'Accept': '*/*',
        'Cookie': '1&_device=android&b273a7a0-a4cc-3d6a-a809-34902d37787a&6.7.3;1&_token=253835232&98CD4FB0340C958ECD9C014AA8918F8983DB15DC92F7B2B09886AFC31BBDDB2D56B087E7A247167M70c84071008FBB4_;channel=and-a1;impl=com.ximalaya.ting.android;osversion=25;fp=009527657x2022q225649v50200000;device_model=ONEPLUS+A5000;XUM=lGUth9UF;XIM=312d36304e722;c-oper=%E4%B8%AD%E5%9B%BD%E8%81%94%E9%80%9A;net-mode=WIFI;freeFlowType=0;res=1080%2C1920;NSUP=;AID=XEIq6zlRE5A=;manufacturer=OnePlus;XD=nIp8pmSl+UW4daM5O+n6tKmURjYecFJUOMHMuyK4q2lSBCXsKpemDZBIJAT8szwCrbk0g6VcZJtbbZoWllCeIr0ESO3IOyT+EqDXrvizU/ueIF1pinMfR7kwWJYnM97kVsGACmK8VT17A6Fh26s1VcsI7f+OqIFZJD1Nz0stN0E=;umid=6c6664a35bae4afb1c39d9a1e5a6f629;xm_grade=0;minorProtectionStatus=0;iccId=89860120801242283007;domain=.ximalaya.com;path=/;',
        'Connection': 'keep-alive',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-cn',
        'User-Agent': 'ting_6.7.3(ONEPLUS+A5000,Android25)'
    }
    response = session.get(url=url, headers=headers)
    print(response.text)
    return response.json()



def adb_forward():
    os.system("adb forward tcp:60008 tcp:60008")


def tt_encrypt(data_str):
    url = 'http://127.0.0.1:60008/ttEncrypt'
    resp = session.post(url, data=data_str.encode('utf-8')).text
    return resp


def tt_encrypt1(data_str):
    url = 'http://127.0.0.1:60008/ttEncrypt1'
    resp = session.post(url, data=data_str.encode('utf-8')).text
    return resp


def download_mp4(url, title):
    headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'ting_6.7.3(ONEPLUS+A5000,Android25)',
        'RANGE': 'bytes=0-'
    }
    response = session.get(url=url, headers=headers)
    fo = open('E:/Text/鬼吹灯/' + title + ".mp3", "wb")
    fo.write(response.content)
    fo.close()


def main():
    adb_forward()
    albumId = '24495603'
    for i in range(1, 13):
        if i == 1:
            response_data = get_list(albumId, i)
            track_id_list = []
            title_list = []
            for i in range(0, len(response_data['data']['tracks']['list'])):
                track_id_list.append(response_data['data']['tracks']['list'][i]['id'])
                title_list.append(response_data['data']['tracks']['list'][i]['title'])
            print(track_id_list)
            print(title_list)
        else:
            response_data = get_list1(albumId, i)
            track_id_list = []
            title_list = []
            for i in range(0, len(response_data['data']['list'])):
                track_id_list.append(response_data['data']['list'][i]['trackId'])
                title_list.append(response_data['data']['list'][i]['title'])
            print(track_id_list)
            print(title_list)
        for i in range(0, len(title_list)):
            response_data = get_downloadurl(track_id_list[i])
            duration = response_data['duration']
            ep = response_data['ep']
            # ep = 'Uj2Ui1+XK7wKS45ocZdhXXFBqWIf5/bEOg7i6FmPN3SYBDqZ679hVqiPOpRRCyqaQwtWPf7v1vjEfGEPxdWsNv5a5Q=='
            ep = tt_encrypt(ep)
            e1_list = ep.split('-')
            buy_key = e1_list[0]
            sign = e1_list[1]
            timestamp = e1_list[3]
            token = e1_list[2]
            fileId = response_data['fileId']
            # fileId = 'MFnfsScZRS108hWgC21/QL6fmU4TjiEExScTEnEtfeu0HtcuYF7FODyD1ZwlYNgO5Mvk4z7hZwFPrefH+nTQbQ=='
            fileId = tt_encrypt1(fileId)
            index = fileId.find("m4a")
            fileId = fileId[0: index+3]
            downloadurl = 'http://audiopay.cos.xmcdn.com/download/1.0.0/{}?buy_key={}&duration={}&sign={}&timestamp={}&token={}&uid=253835232&is_charge=true'.format(fileId, buy_key, duration, sign, timestamp, token)
            print(downloadurl)
            download_mp4(downloadurl, title_list[i])


if __name__ == '__main__':
    main()