package com.example.hookdouyin;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import fi.iki.elonen.NanoHTTPD;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;




public class Hook_XM implements IXposedHookLoadPackage {

    private static HttpServer mserver = null;
    ClassLoader classLoader1 = null;
    Context context2 = null;

    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam arg0) throws Throwable
    {
        if (!arg0.packageName.equals("com.ximalaya.ting.android"))
            return;

        Log.i("Hookximalaya", "[+] HookXM: " + arg0.packageName);


        if(mserver == null){
            try{
                mserver = new HttpServer(60008, null);
                mserver.start();
                XposedBridge.log("[+] toutiao mserver: start ok @60008");
            }catch (Exception e){
                e.printStackTrace();
                XposedBridge.log("[+] toutiao mserver: start error:" + e.toString());
            }
        }

        XposedHelpers.findAndHookMethod(Application.class, "attach", Context.class, new XC_MethodHook()
        {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable
            {

                Context context = (Context) param.args[0];
                classLoader1 = context.getClassLoader();
                hookActivity1(classLoader1);
                hookActivity(classLoader1);
            }
        });
    }



    public void hookActivity1(final ClassLoader classLoader)
    {
        Log.d("Hookximalaya", "[+] HookXM: ");
        XposedHelpers.findAndHookMethod("com.ximalaya.ting.android.encryptservice.EncryptUtil",
                classLoader,
                "HiCwHYxCPw",
                Context.class,
                String.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        mserver.setParam2(param.thisObject.getClass().getName(), param.thisObject);
                        mserver.setParam1("ttContext1", (Context) param.args[0]);
                        Log.d("Hookximalaya", "[+] HookXM: " + param.args[1]);
                    }


                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {

                        super.afterHookedMethod(param);
                        Log.d("Hookximalaya", "[+] HookXM: " + param.getResult());


                    }
                });


        Log.d("Hookximalaya", "[+] HookXM: ");
    }



    public void hookActivity(final ClassLoader classLoader)
    {
        Log.d("Hookximalaya", "[+] HookXM: ");
        XposedHelpers.findAndHookMethod("com.ximalaya.ting.android.host.manager.request.CommonRequestM", classLoader, "init",
                Context.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        super.beforeHookedMethod(param);
                        Log.d("Hookximalaya", "[+] HookXM: "+ param.args[0]);
                        Context context1 = (Context)param.args[0];
                        context2 = context1.getApplicationContext();
                    }

                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {

                        super.afterHookedMethod(param);
                        Log.d("Hookximalaya", "ddddddddddddd");
                        try {
                            Class<?> hook_class = null;
                            hook_class = classLoader1.loadClass("com.ximalaya.ting.android.encryptservice.EncryptUtil");
                            Log.d("Hookximalaya", "loadclass ok");
//                            XposedHelpers.callMethod()
                            Method md = hook_class.getMethod(" ", Context.class, String.class);
                            Method md1 = hook_class.getMethod("b", Context.class, byte[].class);
                            Log.d("Hookximalaya", "getMethod ok");
                            if(md != null)
                            {
                                mserver.setParam("ttEncrypt", md);
                                mserver.setParam("ttEncrypt1", md1);
                                mserver.setParam1("ttContext", context2);
                                XposedBridge.log( "[+] HttpServer: " + "setParam ttEncrypt ok");
                            }
                            else
                            {
                                XposedBridge.log( "[+] HttpServer: " + "setParam md ttEncrypt is null");
                            }
                        } catch (Exception e) {
                            XposedBridge.log("[+] ximalaya: com.bytedance.frameworks.encryptor.EncryptorUtil loaded error");
                        }
                    }
                });


        Log.d("Hookximalaya", "[+] HookXM: ");
    }

}


