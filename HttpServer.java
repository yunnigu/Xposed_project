package com.example.hookdouyin;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import java.util.HashMap;

import fi.iki.elonen.NanoHTTPD;


public class HttpServer extends NanoHTTPD {

    private static final String TAG = "HttpServer";
    private Context ct;
    public HashMap<String, java.lang.reflect.Method> md = new HashMap<>();
    public HashMap<String, android.content.Context> md1 = new HashMap<>();
    public HashMap<String, java.lang.Object> md2 = new HashMap<>();

    public HttpServer(int port, Context context) {
        super(port);
        ct = context;
    }

    public void setParam(String strMethodName, java.lang.reflect.Method method){
        md.put(strMethodName, method);
    }

    public void setParam1(String strMethodName, android.content.Context context){
        md1.put(strMethodName, context);
    }

    public void setParam2(String strMethodName, java.lang.Object obj){
        md2.put(strMethodName, obj);
    }

    @Override
    public Response serve(IHTTPSession session) {

        HashMap<String, String> files = new HashMap<>();
        Method method = session.getMethod();

        if (Method.POST.equals(method)) {
            try {
                session.parseBody(files);
                String postData = files.get("postData");
                String uri = session.getUri().substring(1);
                if(postData != null)
                {
                    java.lang.reflect.Method mdtmp = md.get(uri);
                    java.lang.reflect.Method mdtmp1 = md.get("ttEncrypt1");
                    Context context3 = md1.get("ttContext1");
                    Object obj = md2.get("com.ximalaya.ting.android.encryptservice.EncryptUtil");
                    if(mdtmp == null){
                        return new Response(Response.Status.NOT_FOUND, "text/plain", "No Method or No Inst");
                    }
                    if(mdtmp1 == null){
                        return new Response(Response.Status.NOT_FOUND, "text/plain", "No Method or No Inst");
                    }
                    try{
                        if(uri.equals("ttEncrypt")){
                            byte[] body = postData.getBytes();
                            String body_new = new String(body);
                            Log.d("Hookximalaya", "dddddddd" + body_new);
                            Log.d("Hookximalaya", "dddddddd" + context3);
//                            String bba = (String) mdtmp.invoke(null, context3, body_new);
                            String bba = (String) mdtmp.invoke(obj, context3, body_new);
                            Log.d("Hookximalaya", "dddddddd" + bba);
                            if(bba != null)
                            {
                                return new Response(bba);
                            }

                        }
                        if(uri.equals("ttEncrypt1")){
                            byte[] body1 = postData.getBytes();
                            String body_new1 = new String(body1);
                            byte[] strRes = Base64.decode(body_new1, Base64.DEFAULT);
                            Log.d("Hookximalaya", "dddddddd1" + context3);
//                            String bba = (String) mdtmp.invoke(null, context3, body_new);
                            byte[] bba1 = (byte[]) mdtmp1.invoke(obj, context3, strRes);
                            String bbb = new String(bba1);
                            Log.d("Hookximalaya", "dddddddd" + bbb);
                            if(bbb != null)
                            {
                                return new Response(bbb);
                            }

                        }
                    }catch (Exception e){
                        return new Response(Response.Status.NOT_FOUND, "text/plain", e.getMessage() + "\n\n" + e.toString());
                    }
                }
                return new Response(Response.Status.NOT_FOUND, "text/plain", "No Data");
            }catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "exception:"+e.getMessage());
                Log.e(TAG, "exception:"+e.toString());
            }
        }
        return new Response(Response.Status.NOT_FOUND, "text/plain", "Not Found");
    }


}
